import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchComponent } from './search/search.component';
import { ShowcaseComponent } from './showcase/showcase.component';
import { BookComponent } from './book/book.component';

const routes: Routes = [
  { path: 'showcase', component: ShowcaseComponent, data: {state: 'showcase'}},
  { path: 'book/new', component: BookComponent, data: {state: 'book/new'}},
  { path: 'book/:id', component: BookComponent, data: {state: 'book/:id'}},
  { path: 'search', component: SearchComponent, data: {state: 'search'}},
  { path: '**', redirectTo: '/showcase', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule  { }
