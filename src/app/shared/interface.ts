export interface Book {
  id?: number;
  author: string;
  title: string;
  isbn: string;
  pages: number;
  countryId: number ;
  cityId: number;
  companyId: number;
  formatId: number;
  description: string;
  price: number;
}

export interface FilteredSearch {
  author?: string;
  title?: string;
  isbn?: string;
  pages?: string;
  formatId?: string;
  price?: string;
}

export interface Range {
  min: number;
  max: number;
}

export interface Format {
  id: number;
  name: string;
}
export interface Country {
  id: number;
  name: string;
}
export interface City {
  id: number;
  countryId: number;
  name: string;
}

export interface Company {
  id: number;
  cityId: number;
  name: string;
}
