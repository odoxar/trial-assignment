import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';

import { BookListComponent } from './components/book-list/book-list.component';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { NavigationComponent } from './components/navigation/navigation.component';


@NgModule({
  declarations: [BookListComponent, NavigationComponent],
  imports: [RouterModule, BrowserModule],
  exports: [BookListComponent, NavigationComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ]
})
export class SharedModule {}
