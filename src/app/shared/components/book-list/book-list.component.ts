import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { Book } from '../../interface';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html'
})
export class BookListComponent implements OnInit {
  @Input() booksList: Observable<Book[]>;

  constructor() { }

  ngOnInit() {
  }

}
