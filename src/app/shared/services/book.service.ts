import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CommonService } from './common.service';
import { Book, Format, Company, City, Country } from '../interface';

@Injectable({
  providedIn: 'root'
})
export class BookService {
  constructor(private common: CommonService) {}

  getBooks(): Observable<Book[]> {
    return this.common.getData('books');
  }

  getFormats(): Observable<Format[]> {
    return this.common.getData('formats');
  }

  getCountries(): Observable<Country[]> {
    return this.common.getData('countries');
  }

  getCities(): Observable<City[]> {
    return this.common.getData('cities');
  }

  getCompanies(): Observable<Company[]> {
    return this.common.getData('companies');
  }

  getBookById(id: number): Observable<Book> {
    return this.common.getDataById('books', id);
  }

  getCityById(countryId: number): Observable<City[]> {
    return this.common
      .getData('cities')
      .pipe(
        map((data: City[]) =>
          data.filter((city: City) => city.countryId === countryId)
        )
      );
  }
  getCompanyById(cityId: number): Observable<Company[]> {
    return this.common
      .getData('companies')
      .pipe(
        map((data: Company[]) =>
          data.filter((company: Company) => company.cityId === cityId)
        )
      );
  }

  saveNewBook(data: Book): Observable<Book> {
    return this.common.putData('books', data);
  }
}
