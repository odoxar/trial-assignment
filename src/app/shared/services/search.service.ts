import { Injectable } from '@angular/core';
import { Book, FilteredSearch, Range } from '../interface';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BookService } from './book.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  constructor(private bookService: BookService, private router: Router) {}

  searchByQueryParams(params: FilteredSearch): Observable<Book[]> {
    this.router.navigate(['/search'], { queryParams: params });
    return this.filterBooks(params);
  }

  filterBooks(params: FilteredSearch): Observable<Book[]> {
    return this.bookService.getBooks().pipe(
      map((books: Book[]) => {
        if (!params) {
          return books;
        }
        return books.filter((book: Book) => {
          let matchBetween = true;
          let match = true;
          Object.keys(params).forEach(prop => {
            // check if params have price or pages parametr
            if ((prop === 'pages' || prop === 'price') && matchBetween) {
              matchBetween = this.checkStringData(
                params[prop],
                book[prop]
              );
            } else if (
              // check if search value includes in book
              !book[prop]
                .toString()
                .toLowerCase()
                .includes(params[prop].toString().toLowerCase())
            ) {
              match = false;
            }
          });
          if (!matchBetween && match) {
            return false;
          } else if (
          // check if params is empty
            Object.keys(params).length === 0 &&
            params.constructor === Object
          ) {
            return false;
          }
          return match;
        });
      })
    );
  }

  checkStringData(strToObj: string, bookProp: number): boolean {
    const paramsParse = JSON.parse(strToObj);
    return this.checkBetweenNumbers(bookProp, paramsParse);
  }

  // if book parametrs (price or pages) in the range of min , max
  checkBetweenNumbers(check: number, { min, max }: Range) {
    if (min === null && max !== null) {
      min = min === null ? (min = 0) : min;
      return check <= max;
    } else if (max === null && min !== null) {
      return check >= min;
    } else {
      return check >= min && check <= max;
    }
  }

  // iterate form data to smaller object
  iterateFilter(form: FilteredSearch): FilteredSearch {
    const toSave: FilteredSearch = {};
    for (const prop of Object.keys(form)) {
      if (
        form[prop] !== null &&
        typeof form[prop] !== 'object' &&
        form[prop] !== '' &&
        form[prop] !== 'all'
      ) {
        toSave[prop] = form[prop];
      } else if (form[prop] !== null && typeof form[prop] === 'object') {
        if (!(form[prop].min === null && form[prop].max === null)) {
          toSave[prop] = JSON.stringify(form[prop]);
        }
      }
    }
    return toSave;
  }
}
