import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { concatMap, find } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  private token = 'bad18eba1ff45jk7858b8ae88a77fa30';
  private host = 'http://localhost:3004/';

  constructor(private http: HttpClient) {}

  getToken() {
    return this.token;
  }

  getData<T>(query: string): Observable<T> {
    return this.http.get<T>(`${this.host}${query}`);
  }
  getDataById<T>(query: string, id: number): Observable<T> {
    return this.getData(query).pipe(
      concatMap((data: T[]) => data),
      find((item: any) => {
        return item.id === id;
      })
    );
  }

  putData<T>(query: string, data: T): Observable<T> {
    return this.http.post<T>(`${this.host}${query}`, data);
  }
}
