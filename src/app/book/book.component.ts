import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {
  FormGroup,
  Validators,
  FormBuilder
} from '@angular/forms';
import { Book, Format, Country, City, Company } from '../shared/interface';
import { BookService } from '../shared/services/book.service';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styles: [`
    .error-position {
      right: 0;
    },
    .form-control-disabledtext {
      background-color: transparent;
    }
    `]
})
export class BookComponent implements OnInit {
  public form: FormGroup;
  public hideBtn = true;
  public modal = false;
  public bookFormatId: number;
  public bookCountryId: number;
  public bookCityId: number;
  public bookCompanyId: number;

  public book;
  public formats: Observable<Format[]>;
  public countries: Observable<Country[]>;
  public cities: Observable<City[]>;
  public companies: Observable<Company[]>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private bookService: BookService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.initForm();
    this.route.params
      .pipe(
        switchMap((params: Params) => {
          if (params.id) {
            this.form.disable();
            this.hideBtn = false;
            this.cities = this.bookService.getCities();
            this.companies = this.bookService.getCompanies();

            return this.bookService.getBookById(+params.id);
          }
          return of(null);
        })
      )
      .subscribe(book => {
        this.setForm(book);
        // getting info from server
        this.bookFormatId = this.form.get('bookInfoGroup').value.format;
        this.bookCountryId = this.form.get('publisherGroup').value.country;
        this.bookCityId = this.form.get('publisherGroup').value.city;
        this.bookCompanyId = this.form.get('publisherGroup').value.company;
        // disabled select city and company while user get country
        this.form.get('publisherGroup.city').disable();
        this.form.get('publisherGroup.company').disable();

      });

    this.formats = this.bookService.getFormats();
    this.countries = this.bookService.getCountries();
  }

  initForm() {
    this.form = this.fb.group({
      bookInfoGroup: this.fb.group({
        author: ['', [Validators.required]],
        title: ['', [Validators.required]],
        isbn: ['', [Validators.required, Validators.pattern('^\\d{10}(\\d{3})?$')]],
        pages: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
        format: ['', [Validators.required]],
        description: ['', [Validators.required]],
        price: ['', [Validators.required, Validators.pattern('^[0-9]*$')]]
      }),
      publisherGroup: this.fb.group({
        country: ['', [Validators.required]],
        city: ['', [Validators.required]],
        company: ['', [Validators.required]]
      })
    });
  }

  filterCity(id: number) {
    this.form.get('publisherGroup.city').enable();
    this.cities = this.bookService.getCityById(id);
  }

  filterCompany(id: number) {
    this.form.get('publisherGroup.company').enable();
    this.companies = this.bookService.getCompanyById(id);
  }

  setForm(book: Book) {
    if (book) {
      this.form.patchValue({
        bookInfoGroup: {
          author: book.author,
          title: book.title,
          isbn: book.isbn,
          pages: book.pages,
          format: book.formatId,
          description: book.description,
          price: book.price
        },
        publisherGroup: {
          country: book.countryId,
          city: book.cityId,
          company: book.companyId
        }
      });
    }
  }

  onSubmit(formBook) {
    const formBookInfo = formBook.bookInfoGroup;
    const formPublisher = formBook.publisherGroup;
    const newBook: Book = {
      author: formBookInfo.author,
      title: formBookInfo.title,
      isbn: formBookInfo.isbn,
      pages: +formBookInfo.pages,
      countryId: +formPublisher.country,
      cityId: +formPublisher.city,
      companyId: +formPublisher.company,
      formatId: +formBookInfo.format,
      description: formBookInfo.description,
      price: +formBookInfo.price,
    };

    this.bookService.saveNewBook(newBook)
      .subscribe(
        res => {
          this.book = res;
          this.modal = true;
          setTimeout(() => {
            this.router.navigate(['/showcase']);
          }, 1500);
      },
      error => console.error(error)
    );

    this.form.reset();
    this.form.get('publisherGroup.city').disable();
    this.form.get('publisherGroup.company').disable();
  }
}
