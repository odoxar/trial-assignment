import { Component, OnInit } from '@angular/core';
import { BookService } from '../shared/services/book.service';
import { Observable } from 'rxjs';
import { Book } from '../shared/interface';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-showcase',
  templateUrl: './showcase.component.html'
})
export class ShowcaseComponent implements OnInit {
  public books: Observable<Book[]>;

  constructor(private bookService: BookService) { }

  ngOnInit() {
    this.books = this.bookService.getBooks().pipe(
      delay(1000)
    );
  }



}
