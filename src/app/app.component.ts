import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { routerTransition } from './shared/main.animation';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: [
    `
      :host {
        display: flex;
        flex-direction: column;
        height: 100vh;
      }
      :host main {
        flex: 1;
        position: relative;
      }
      :host /deep/ router-outlet ~ * {
        position: absolute;
        width: 100%;
        height: 100%;
      }
    `
  ],
  animations: [routerTransition]
})
export class AppComponent {
  getState(outlet: RouterOutlet) {
    return outlet.activatedRouteData.state;
  }
}
