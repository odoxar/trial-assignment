import { Component, OnInit } from '@angular/core';
import { BookService } from '../shared/services/book.service';
import { Book, Format, FilteredSearch } from '../shared/interface';
import { Observable } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';
import {
  debounceTime,
  distinctUntilChanged,
  map,
  delay
} from 'rxjs/operators';
import { SearchService } from '../shared/services/search.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html'
})
export class SearchComponent implements OnInit {
  public searchForm: FormGroup;
  public formats: Observable<Format[]>;
  public books: Observable<Book[]>;

  constructor(
    private bookService: BookService,
    private fb: FormBuilder,
    private searchService: SearchService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.initForm();
    this.formats = this.bookService.getFormats();
    // getting params from url
    this.route.queryParams.subscribe((params: FilteredSearch) => {
      if (
        Object.entries(params).length !== 0 &&
        params.constructor === Object
      ) {
        this.books = this.searchService.searchByQueryParams(params).pipe(
          delay(1000)
        );
      }
    });
    this.getFormData();
  }

  initForm() {
    this.searchForm = this.fb.group({
      author: [null],
      title: [null],
      isbn: [
        null,
      ],
      formatId: 'all',
      pages: this.fb.group({
        min: null,
        max: null
      }),
      price: this.fb.group({
        min: null,
        max: null
      })
    });
  }

  getFormData() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(400),
        distinctUntilChanged(),
        map((form: FilteredSearch) => {
          return this.searchService.iterateFilter(form);
        })
      )
      .subscribe((form: FilteredSearch) => {
        this.books = this.searchService
          .searchByQueryParams(form)
          .pipe(delay(1000));

      });
  }
}

